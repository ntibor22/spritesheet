package fw.anim
{
	import flash.display.BitmapData;

	public class TileSheetHelper
	{
		public function prepareAnimTexture(texture:BitmapData, sheetData:Object, isStarling:Boolean):AnimTextureSheet
		{
			var animData:Array= [];
			if (isStarling) {
				// Build from xml
				var seqXML:XML = XML(sheetData);
				populateFrameArrayFromXml(animData, seqXML);
			}
			else {
				var seqRaw:Object =  JSON.parse( sheetData as String);
				populateFrameArray(animData, seqRaw.frames);
			}
			
			
			// Sort
			animData.sortOn("id");
				
			// Create and initialize the tile sheet
			var tileSheet:AnimTextureSheet= new AnimTextureSheet();
			tileSheet.init(texture, animData);
			
			return tileSheet;
		}
		
		
		
		private function fixIndex(src:String, delim:String):String
		{
			var parts:Array = src.split(delim);
			if (parts.length != 2) return src;
			
			var zeros:String= "0000";
			var indStr:String =  String(parts[1]);
			var len:int =indStr.length;
			
			return String(parts[0])+ delim+ zeros.substr(0, 4-len) + indStr;
		}
			
		private function populateFrameArray(arDest:Array, src:Object):void
		{
			var entry:Object;
			var nameId:String;
			var item:Object;
			
			for(var keyName:String in src) {
				nameId= keyName.substring(0, keyName.lastIndexOf(".png"));
				
				entry= src[keyName];
				
				item = entry.frame;
				item.id = fixIndex(nameId, "_");
				
				item.offX = 0;
				item.offY = 0;
				if (entry.trimmed) {
					item.offX= entry.spriteSourceSize.x;
					item.offY= entry.spriteSourceSize.y;
				}
				
				
				arDest.push(item);
			}
		}
		
		// Parse a Sparrow/Starling format XML
		//
		private function populateFrameArrayFromXml(arDest:Array, sheetXML:XML):void
		{
			var scale:Number = 1.0;
			
			var nameId:String;
			var item:Object;
			
			for each (var subTexture:XML in sheetXML.SubTexture)
			{
				var name:String        = subTexture.attribute("name");
				var x:Number           = parseFloat(subTexture.attribute("x")) / scale;
				var y:Number           = parseFloat(subTexture.attribute("y")) / scale;
				var width:Number       = parseFloat(subTexture.attribute("width")) / scale;
				var height:Number      = parseFloat(subTexture.attribute("height")) / scale;
				var frameX:Number      = parseFloat(subTexture.attribute("frameX")) / scale;
				var frameY:Number      = parseFloat(subTexture.attribute("frameY")) / scale;
				var frameWidth:Number  = parseFloat(subTexture.attribute("frameWidth")) / scale;
				var frameHeight:Number = parseFloat(subTexture.attribute("frameHeight")) / scale;
				
				
				item= new Object();
				item.x= x; item.y= y;
				item.w= width; item.h= height;
				item.id = name;
				
				item.offX = -frameX;
				item.offY = -frameY;
				
				arDest.push(item);
			}
			
		}
	}
}