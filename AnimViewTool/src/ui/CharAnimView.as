package ui
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	
	import fw.anim.AnimSprite;
	
	import model.AppModel;
	
	public class CharAnimView extends Sprite
	{
		private var _appModel:AppModel;
		private var _charAnim:AnimSprite;
		private var _panel:OptionsPanel;
		
		private var curTime:int=0;
		private var lastTime:int=0;
		
		public function CharAnimView(model:AppModel)
		{
			super();
			
			_appModel= model;
			addEventListener(Event.ADDED_TO_STAGE, onAdded);
			
			
		}
		
		protected function onAdded(event:Event):void
		{
			createPanel(parent);
			
			curTime= getTimer();
			lastTime= curTime;
		}
		
		public function initAnim(anim:AnimSprite):void
		{
			_charAnim= anim;
			_charAnim.smoothing= true;
			
			addChild(_charAnim);
			
			//_charAnim.play("idle");
			//_charAnim.updateAnimation();
			
			centerView();
			drawBounds(true);
			
			_panel.setInfo(_charAnim.numFrames, _charAnim);
			
			addEventListener(Event.ENTER_FRAME, onFrameUpdate);
		}
		
		public function centerView():void
		{
			if (_charAnim) {
				x = (510 - _charAnim.bitmapData.rect.width * scaleX) /2;
				y = (660 - _charAnim.bitmapData.rect.height * scaleY) /2;
			}
		
		}
		
		public function setFrameRate(fps:int):void
		{
			if (_charAnim && _charAnim.fps != fps) {
				_charAnim.forceFrameRate(fps);
				_appModel.log("Frame rate set to " + fps + " FPS");
			}
		}
		
		private function createPanel(rootApp:DisplayObjectContainer):void
		{
			_panel = new OptionsPanel(_appModel);
			_panel.x= 510; _panel.y=24;
			rootApp.addChild(_panel);
			
			_panel.createGUI(this);
			
		}		
		
		public function drawBounds(show:Boolean):void
		{
			graphics.clear();
			if (!show) {
				return;
			}
			
			var rc:Rectangle = _charAnim.bitmapData.rect;
			graphics.lineStyle(0.5,0);
			graphics.drawRect(rc.x, rc.y, rc.width, rc.height);
		}
		
		
		protected function onFrameUpdate(event:Event):void
		{
			curTime = getTimer()
			
			_charAnim.updateAnimation(curTime-lastTime);
			lastTime= curTime;
			
			_panel.updateFrame();
		}
		
			
	}
}